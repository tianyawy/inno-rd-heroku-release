#!/bin/bash
set -e
rm -rf innoRD-api
git clone git@gitlab.com:daicode/innoRD-api.git
cd innoRD-api
sh -u deploy-to-heroku.sh
